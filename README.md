# Docker Workshop
Lab 12: Building and running a microservices application

---

## Preparations

 - Create a new folder for the lab:
```
$ mkdir ~/lab-12
$ cd ~/lab-12
```

## Instructions

 - Clone the sum service repository and build the docker image:

```
 $ git clone https://gitlab.com/npm-microservices/sum-service.git ~/lab-12/sum-service
 $ cd ~/lab-12/sum-service
 $ docker build -t sum-service:latest .
```

 - Clone the subtraction service repository and build the docker image:

```
 $ git clone https://gitlab.com/npm-microservices/subtraction-service.git ~/lab-12/subtraction-service
 $ cd ~/lab-12/subtraction-service
 $ docker build -t subtraction-service:latest .
```

 - Clone the multiplication service repository and build the docker image:

```
 $ git clone https://gitlab.com/npm-microservices/multiplication-service.git ~/lab-12/multiplication-service
 $ cd ~/lab-12/multiplication-service
 $ docker build -t multiplication-service:latest .
```

 - Clone the division service repository and build the docker image:

```
 $ git clone https://gitlab.com/npm-microservices/division-service.git ~/lab-12/division-service
 $ cd ~/lab-12/division-service
 $ docker build -t division-service:latest .
```

 - Clone the ui service repository and build the docker image:

```
 $ git clone https://gitlab.com/npm-microservices/ui-service.git ~/lab-12/ui-service
 $ cd ~/lab-12/ui-service
 $ docker build -t ui-service:latest .
```

 - Create a new folder to store the docker-compose file:
```
$ mkdir ~/lab-12/calculator-app
$ cd ~/lab-12/calculator-app
```

- Create a "docker-compose.yml" file to deploy the application with the content below:

```
version: '3'
services:
  sum-service:
    image: sum-service:latest
    container_name: sum-service
    restart: always
    ports:
     - "3001:3000"
    networks:
     - "npm-microservices"
  subtraction-service:
    image: subtraction-service:latest
    container_name: subtraction-service
    restart: always
    ports:
     - "3002:3000"
    networks:
     - "npm-microservices"
  multiplication-service:
    image: multiplication-service:latest
    container_name: multiplication-service
    restart: always
    ports:
     - "3003:3000"
    networks:
     - "npm-microservices"
  division-service:
    image: division-service:latest
    container_name: division-service
    restart: always
    ports:
     - "3004:3000"
    networks:
     - "npm-microservices"
  ui-service:
    image: ui-service:latest
    container_name: ui-service
    environment:
      - HOST_IP=<server-ip>
    restart: always
    ports:
     - "3000:3000"
    networks:
     - "npm-microservices"
networks:
  npm-microservices:
```

 - Ensure that you replaced "<server-ip>" in the environment varibale "HOST_IP" under the "ui-service"
 
 - Deploy the microservices application:

```
 $ docker-compose up -d
```

 - Browse to the application (ui service):

```
http://<server-ip>:3000
```

## Cleanup

 - Stop the application

```
docker-compose down
```

 - Remove images and containers:

```
docker rm -f $(docker ps -a -q)
docker rmi $(docker images -q)
```
